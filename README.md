# README #

Mocha Sample

### 如何安裝 ###
```
$ cd {app folder}
$ npm install
```

### 如何啟動 ###
```
$ cd {app folder}
$ npm start
```

### 如何測試 ###
```
$ cd {app folder}
$ npm test
```

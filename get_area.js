module.exports = function(width , height){
  if (isNaN(width)){
    return 0;
  }
  if (!height){
    return width * width;
  }
  
  if (isNaN(height)){
    return 0;
  }
  return width * height;
}

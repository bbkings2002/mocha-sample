var expect = require("chai").expect;
var get_area = require('../get_area');

describe('Get Area Testing', function () {
  it ("isNaN Testing" , function(){
    var width = "abc";
    expect(get_area(width)).to.equal(0);

    var width = false;
    expect(get_area(width)).to.equal(0);

    var width = 10.24;
    expect(get_area(width)).to.equal(0);

  });

  it("Rectangle Testing" , function(){
    var width = 20;
    var height = 10;
    expect(get_area(width,height)).to.equal(width*height);
  });

  it("Square Testing" , function(){
    var length = 20;
    expect(get_area(length)).to.equal(length*length);
  });
});
